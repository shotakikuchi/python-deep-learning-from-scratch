# coding: utf-8
import numpy as np
import matplotlib.pylab as plt
import sys, os

sys.path.append(os.pardir)  # 親ディレクトリのファイルをインポートするための設定
from common.gradient import numerical_gradient, gradient_descent


# [2,3] => [4,9] 1次元配列のインデックス1,2を2乗して返す
def function_2(x):
  return x[0] ** 2 + x[1] ** 2
  # または return np.sum(x**2)


init_x = np.array([-3.0, 4.0])

result = gradient_descent(function_2, init_x, lr=0.1, step_num=100)
print(result)
