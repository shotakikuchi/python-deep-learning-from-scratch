# coding: utf-8
import sys, os

sys.path.append(os.pardir)  # 親ディレクトリのファイルをインポートするための設定
import numpy as np

from common.functions import cross_entropy_error
from common.gradient import gradient_descent


def function_2(x):
  return x[0] ** 2 + x[1] ** 2


init_x = np.array([-3.0, 4.0])
result = gradient_descent(function_2, init_x, lr=0.1, step_num=100)
print(result)
