# coding: utf-8
import numpy as np
import matplotlib.pylab as plt


# 数値微分
def numerical_diff(f, x):
  h = 1e-4  # 0.0001
  return (f(x + h) - f(x - h)) / (2 * h)


def function_1(x):
  return x ** 2 + 0.1 * x


x = np.arange(0.0, 20.0, 0.1)  # 0から20まで、0.1刻みのx配列
y = function_1(x)


def tangent_line(f, x):
  d = numerical_diff(f, x)
  print(d)
  # 切片(0地点でのy座標を求めるので現在のx=5のy座標からxを5戻した数値が切片となる。傾きはd)
  y = f(x) - d * x
  print(y)
  return lambda t: d * t + y


plt.xlabel("x")
plt.ylabel("f(x)")

tf = tangent_line(function_1, 5)
y2 = tf(x)

plt.plot(x, y)
plt.plot(x, y2)
plt.show()
