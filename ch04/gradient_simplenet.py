# coding: utf-8
import numpy as np
import matplotlib.pylab as plt
import sys, os

sys.path.append(os.pardir)  # 親ディレクトリのファイルをインポートするための設定
from common.gradient import numerical_gradient, gradient_descent
from common.functions import softmax, cross_entropy_error


class simpleNet:
  def __init__(self):
    self.W = np.random.randn(2, 3)  # ガウス分布で初期化

  def predict(self, x):
    return np.dot(x, self.W)

  def loss(self, x, t):
    z = self.predict(x)
    y = softmax(z)
    loss = cross_entropy_error(y, t)

    return loss


net = simpleNet()
x = np.array([0.6, 0.9])
p = net.predict(x)
t = np.array([0, 0, 1])  # 正解ラベル
loss = net.loss(x, t)


def f(W):
  return net.loss(x, t)


dW = numerical_gradient(f, net.W)
print(dW)
