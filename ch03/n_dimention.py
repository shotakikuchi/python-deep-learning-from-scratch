# coding: utf-8
import numpy as np

# 1次元
A = np.array([1.0, 2.0, 3.0])
print(np.ndim(A))  # 次元数
print(np.shape(A))  # 配列の形状

B = np.array([[1.0, 2.0], [3.0, 4.0], [5.0, 6.0]])
print(np.ndim(B))  # 次元数
print(np.shape(B))  # 配列の形状
