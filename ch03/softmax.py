# coding: utf-8

import numpy as np


def softmax(a):
  c = np.max(a)  # オーバーフロー対策
  exp_a = np.exp(a - c)  # 指数関数
  # print(exp_a)
  # [  1.34985881  18.17414537  54.59815003]


  sum_exp_a = np.sum(exp_a)
  # print(sum_exp_a)

  y = exp_a / sum_exp_a

  return y


a = np.array([0.3, 2.9, 4.0])
y = softmax(a)
print(y)
print(np.sum(y))
