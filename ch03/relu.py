# coding: utf-8

import numpy as np
import matplotlib.pyplot as plt


# 0以下は0を、0以上は値をそのまま返す
def relu(x):
  return np.maximum(0, x)


x = np.arange(-2.0, 5.0, 0.1)
y = relu(x)

plt.plot(x, y)
plt.ylim(-1.0, max(x))
plt.title("Relu function")
plt.show()
