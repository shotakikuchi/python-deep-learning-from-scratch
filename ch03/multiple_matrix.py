# coding: utf-8

import numpy as np

a = np.array([[1, 2], [3, 4]])
print(a.shape)
b = np.array([[5, 6], [7, 8]])
print(b.shape)

print(np.dot(a, b))

c = np.array([[1, 2, 3], [4, 5, 6]])
print(c.shape)
d = np.array([[1, 2], [3, 4], [5, 6]])
print(d.shape)

print(np.dot(c, d))
print(np.dot(d, c))
