# coding: utf-8
import sys, os

sys.path.append(os.pardir)  # 親ディレクトリのファイルをインポートするための設定
import numpy as np

from common.functions import cross_entropy_error
from common.util import im2col
from dataset.mnist import load_mnist

x1 = np.random.rand(1, 3, 7, 7)  # ランダムにデータを生成 (1個のデータ)
col1 = im2col(x1, 5, 5, stride=1, pad=0)
print("col2.shape", col1.shape)  # (9, 75)

x2 = np.random.rand(10, 3, 7, 7)  # 10個のデータ
col2 = im2col(x2, 5, 5, stride=1, pad=0)
print("col2.shape", col2.shape)  # (90, 75)
